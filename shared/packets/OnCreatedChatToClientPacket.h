#pragma once

#include <utility>

#include "../CommonPacket.h"

class OnCreatedChatToClientPacket : public CommonPacket {
public:
    OnCreatedChatToClientPacket() = default;

    explicit OnCreatedChatToClientPacket(uint32_t chatId) : chatId(chatId) {}

    void write(binary_oarchive& serialize) override {
        serialize << chatId;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> chatId;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    uint32_t chatId;
};
