#pragma once

#include <utility>

#include "../CommonPacket.h"

class AddUserToChatToServerPacket : public CommonPacket {
public:
    AddUserToChatToServerPacket() = default;

    AddUserToChatToServerPacket(uint32_t chatId, std::string invited) : chatId(chatId), invited(std::move(invited)) {}

    void write(binary_oarchive& serialize) override {
        serialize << chatId << invited;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> chatId >> invited;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    uint32_t chatId{};
    std::string invited;
};