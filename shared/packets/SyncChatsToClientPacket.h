#pragma once

#include <utility>
#include <vector>

#include "../CommonPacket.h"
#include "../user/Chat.h"

class SyncChatsToClientPacket : public CommonPacket {
public:
    SyncChatsToClientPacket() = default;

    explicit SyncChatsToClientPacket(const std::vector<std::shared_ptr<Chat>> &chat_ptrs) {
        for (const auto &chat_ptr : chat_ptrs) {
            chats.emplace_back(*chat_ptr);
        }
    }

    void write(binary_oarchive& serialize) override {
        serialize << chats;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> chats;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    std::vector<Chat> chats{};
};