#pragma once

#include <utility>

#include "../CommonPacket.h"

class StartChatWithToServerPacket : public CommonPacket {
public:
    StartChatWithToServerPacket() = default;

    StartChatWithToServerPacket(std::string invited) : invited(std::move(invited)) {}

    void write(binary_oarchive& serialize) override {
        serialize << invited;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> invited;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    std::string invited;
};