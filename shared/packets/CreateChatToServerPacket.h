#pragma once

#include <utility>

#include "../CommonPacket.h"

class CreateChatToServerPacket : public CommonPacket {
public:
    CreateChatToServerPacket() = default;

    void write(binary_oarchive& serialize) override {
    }

    void read(binary_iarchive& serialize) override {
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;
};