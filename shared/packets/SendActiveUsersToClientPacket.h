#pragma once

#include <utility>

#include "../CommonPacket.h"

class SendActiveUsersToClientPacket : public CommonPacket {
public:
    SendActiveUsersToClientPacket() = default;

    explicit SendActiveUsersToClientPacket(std::vector<std::string> users) : users(std::move(users)) {}

    void write(binary_oarchive& serialize) override {
        serialize << users;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> users;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    std::vector<std::string> users;
};