#pragma once

#include <utility>

#include "../CommonPacket.h"

class LogInToServerPacket : public CommonPacket {
public:
    LogInToServerPacket() = default;

    LogInToServerPacket(const std::string& userid, const std::string& password) : userid(userid), password(password) {}

    void write(binary_oarchive& serialize) override {
        serialize << userid;
        serialize << password;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> userid;
        serialize >> password;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    std::string userid;
    std::string password;
};