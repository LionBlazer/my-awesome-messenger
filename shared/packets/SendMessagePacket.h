#pragma once

#include <utility>

#include "../CommonPacket.h"
#include "../user/ChatMessage.h"

class SendMessagePacket : public CommonPacket {
public:
    SendMessagePacket() = default;

    SendMessagePacket(int32_t chatId, const ChatMessage &message) : chat_id(chatId), message(message) {}

    void write(binary_oarchive& serialize) override {
        serialize << chat_id;
        serialize << message;
    }

    void read(binary_iarchive& serialize) override {
        serialize >> chat_id;
        serialize >> message;
    }

    void onReceived(const ip::basic_endpoint<tcp>& from) override;

    int32_t chat_id{};
    ChatMessage message{};
};