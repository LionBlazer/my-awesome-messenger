#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>

struct ChatMessage {
    ChatMessage() = default;

    ChatMessage(const ChatMessage& other) {
        messageId = other.messageId;
        senderId = other.senderId;
        message = other.message;
    }

    ChatMessage& operator=(const ChatMessage& other) = default;

    int messageId = -1;
    std::string senderId;
    std::string message;

private:
    friend class boost::serialization::access;

    template <typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & messageId;
        ar & senderId;
        ar & message;
    }
};