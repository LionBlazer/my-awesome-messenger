#pragma once
#include "ChatMessage.h"
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_guard.hpp>
#include <vector>

using namespace boost;
using namespace boost::archive;

class Chat {
public:
    Chat() = default;

    Chat(const Chat& other) {
        chat_id = other.chat_id;
        received = std::vector(other.received);
    }

    Chat& operator=(const Chat& other) {
        chat_id = other.chat_id;
        received = std::vector(other.received);
        return *this;
    }

    void addMessageSafe(ChatMessage& message) {
        boost::lock_guard<boost::mutex> lock(mtx);
        message.messageId = int(received.size());
        received.push_back(message);
    }

    int32_t chat_id = 0;
    std::vector<ChatMessage> received{};
private:
    friend class boost::serialization::access;

    template <typename Archive>
    void serialize(Archive &ar, const unsigned int version) {
        ar & chat_id;
        ar & received;
    }

    boost::mutex mtx{};
};