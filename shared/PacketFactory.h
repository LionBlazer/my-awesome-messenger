#pragma once
#include "CommonPacket.h"
#include <iostream>
#include <map>
#include <memory>
#include <functional>
#include <typeindex>
#include <utility>

class PacketFactory {
public:
    using PacketCreator = std::function<std::unique_ptr<CommonPacket>()>;

    static PacketFactory& instance() {
        static PacketFactory registry;
        return registry;
    }


    void registerPacket(int id, PacketCreator creator, const std::type_index& type) {
        creators[id] = std::move(creator);
        typeToId[type] = id;
    }

    [[nodiscard]] std::unique_ptr<CommonPacket> create(int id) const {
        auto creator = creators.find(id);
        if (creator != creators.end()) {
            return creator->second();
        }
        return nullptr;
    }

    [[nodiscard]] int getPacketId(const CommonPacket& packet) const {
        auto typeId = std::type_index(typeid(packet));
        auto idIter = typeToId.find(typeId);
        if (idIter != typeToId.end()) {
            return idIter->second;
        }
        return -1;
    }

private:
    std::map<int, PacketCreator> creators;
    std::map<std::type_index, int> typeToId;
};

std::vector<std::uint8_t>* CommonPacket::serialize(CommonPacket* packet) {
    const auto& factory = PacketFactory::instance();

    std::stringstream ss;
    boost::archive::binary_oarchive oa(ss);

    oa << factory.getPacketId(*packet);
    packet->write(oa);

    const std::string& tmp_str = ss.str();
    auto data = new std::vector<std::uint8_t>(tmp_str.begin(), tmp_str.end());

    delete packet;

    return data;
}

std::unique_ptr<CommonPacket> CommonPacket::deserialize(std::vector<std::uint8_t>* packet) {
    const auto& factory = PacketFactory::instance();
    std::stringstream ss(std::string(packet->begin(), packet->end()));
    boost::archive::binary_iarchive ia(ss);

    int id;
    ia >> id;

    auto common_packet = factory.create(id);
    if (common_packet) {
        common_packet->read(ia);
    }

    delete packet;
    return common_packet;
}