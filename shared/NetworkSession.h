#pragma once

#include "CommonPacket.h"
#include <boost/asio.hpp>
#include <boost/asio/ts/buffer.hpp>
#include <boost/asio/ts/internet.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <utility>

using namespace boost;
using asio::ip::tcp;
using ssl_socket = asio::ssl::stream<tcp::socket>;

class NetworkSession : public std::enable_shared_from_this<NetworkSession> {
public:
    explicit NetworkSession(std::shared_ptr<ssl_socket> ssl_sock) : ssl_sock_(std::move(ssl_sock)), received(128), to_send(128) {}

    void startReceivingPackets();

    void sendCurrentPackets();

    void addToSendingQueue(CommonPacket* packet);

    bool closed = false;
    boost::lockfree::queue<CommonPacket *> received;
private:
    std::shared_ptr<ssl_socket> ssl_sock_;
    uint32_t current_packet_length{};
    uint32_t current_send_packet_length{};
    bool sending_blocked{};

    boost::lockfree::queue<CommonPacket *> to_send;

    void on_length_read(system::error_code error, std::size_t);

    void on_data_read(std::vector<std::uint8_t> *data, system::error_code error, std::size_t);

    void on_length_written(std::vector<std::uint8_t> *packet, system::error_code error, std::size_t);

    void on_data_written(std::vector<std::uint8_t> *packet, system::error_code error, std::size_t);
};