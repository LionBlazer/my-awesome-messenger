#include "NetworkSession.h"
#include <functional>

using namespace std::placeholders;

//region receiving

void NetworkSession::startReceivingPackets() {
    asio::async_read(
            *ssl_sock_, asio::buffer(&current_packet_length, sizeof(uint32_t)),
            std::bind(&NetworkSession::on_length_read, shared_from_this(), _1, _2)
    );
}

void NetworkSession::on_length_read(system::error_code error, std::size_t) {
    if (!error) {
        auto data = new std::vector<std::uint8_t>(current_packet_length);
        asio::async_read(
                *ssl_sock_, asio::buffer(*data, current_packet_length),
                std::bind(&NetworkSession::on_data_read, shared_from_this(), data, _1, _2)
        );
    } else {
        closed = true;
        std::cerr << error.message() << std::endl;
    }
}

void NetworkSession::on_data_read(std::vector<std::uint8_t> *data, system::error_code error, std::size_t) {
    if (!error) {
        received.push(CommonPacket::deserialize(data).release());
        startReceivingPackets();
    }else {
        closed = true;
        std::cerr << error.message() << std::endl;
    }
}

//endregion

//region sending

void NetworkSession::sendCurrentPackets() {
    if (sending_blocked) return;

    if (!to_send.empty()) {
        sending_blocked = true;

        CommonPacket* packet;
        to_send.pop(packet);

        auto data = CommonPacket::serialize(packet);
        current_send_packet_length = data->size();


        asio::async_write(
                *ssl_sock_, asio::buffer(&current_send_packet_length, sizeof(uint32_t)),
                std::bind(&NetworkSession::on_length_written, shared_from_this(), data, _1, _2)
        );
    }
}

void NetworkSession::on_length_written(std::vector<std::uint8_t>* packet, system::error_code error, std::size_t) {
    if (!error) {
        asio::async_write(
                *ssl_sock_, asio::buffer(&(*(packet->begin())), packet->size()),
                std::bind(&NetworkSession::on_data_written, shared_from_this(), packet, _1, _2)
        );
    } else {
        closed = true;
        std::cerr << error.message() << std::endl;
    }

    // не принципиально если тред который отправляет пакеты обновит флажок позже
    sending_blocked = false;
}

void NetworkSession::on_data_written(std::vector<std::uint8_t> *packet, system::error_code error, std::size_t) {
    if (error) {
        closed = true;
        std::cerr << error.message() << std::endl;
    }

    delete packet;
    sending_blocked = false;
}

//endregion

void NetworkSession::addToSendingQueue(CommonPacket* packet) {
    to_send.push(packet);
}
