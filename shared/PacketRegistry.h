#pragma once

#include "PacketFactory.h"
#include "packets/LogInToServerPacket.h"
#include "packets/SendMessagePacket.h"
#include "packets/SyncChatsToClientPacket.h"
#include "packets/CreateChatToServerPacket.h"
#include "packets/AddUserToChatToServerPacket.h"
#include "packets/RequestActiveUsersToServerPacket.h"
#include "packets/SendActiveUsersToClientPacket.h"
#include "packets/OnCreatedChatToClientPacket.h"
#include "packets/StartChatWithToServerPacket.h"

namespace PacketRegistry {
    void registerPackets() {
        PacketFactory &registry = PacketFactory::instance();
        registry.registerPacket(0, []() { return std::make_unique<LogInToServerPacket>(); }, typeid(LogInToServerPacket));
        registry.registerPacket(1, []() { return std::make_unique<SendMessagePacket>(); }, typeid(SendMessagePacket));
        registry.registerPacket(2, []() { return std::make_unique<SyncChatsToClientPacket>(); }, typeid(SyncChatsToClientPacket));
        registry.registerPacket(3, []() { return std::make_unique<CreateChatToServerPacket>(); }, typeid(CreateChatToServerPacket));
        registry.registerPacket(4, []() { return std::make_unique<AddUserToChatToServerPacket>(); }, typeid(AddUserToChatToServerPacket));
        registry.registerPacket(5, []() { return std::make_unique<RequestActiveUsersToServerPacket>(); }, typeid(RequestActiveUsersToServerPacket));
        registry.registerPacket(6, []() { return std::make_unique<SendActiveUsersToClientPacket>(); }, typeid(SendActiveUsersToClientPacket));
        registry.registerPacket(7, []() { return std::make_unique<OnCreatedChatToClientPacket>(); }, typeid(OnCreatedChatToClientPacket));
        registry.registerPacket(8, []() { return std::make_unique<StartChatWithToServerPacket>(); }, typeid(StartChatWithToServerPacket));
    }
}