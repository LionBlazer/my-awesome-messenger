#pragma once
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/asio/ts/internet.hpp>

using namespace boost::archive;
namespace ip = boost::asio::ip;
using ip::tcp;

class CommonPacket {
public:
    virtual ~CommonPacket() = default;
    static std::vector<std::uint8_t>* serialize(CommonPacket* packet);
    static std::unique_ptr<CommonPacket> deserialize(std::vector<std::uint8_t>* packet);

    virtual void write(binary_oarchive& serialize) = 0;
    virtual void read(binary_iarchive& serialize) = 0;

    /**
     * Executing when packet has been delivered in the main stream on the receiving side
     */
    virtual void onReceived(const ip::basic_endpoint<tcp>& from) = 0;
};