#pragma once
#include "network/ClientConnectionManager.h"
#include "gui/MessengerGui.h"

struct Client {
    static Client& instance() {
        static Client server;
        return server;
    }

    std::shared_ptr<ClientConnectionManager> connectionManager = std::make_shared<ClientConnectionManager>();
    std::shared_ptr<MessengerGui> messengerGui = std::make_shared<MessengerGui>();
};