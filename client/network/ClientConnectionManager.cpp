#include "ClientConnectionManager.h"

using namespace std::placeholders;

void ClientConnectionManager::connectAsync(const std::string &host, const std::string &port) {
    auto self(shared_from_this());

    ssl_context.set_options(
            asio::ssl::context::default_workarounds
            | asio::ssl::context::no_sslv3
            | asio::ssl::context::single_dh_use);
    ssl_context.set_verify_mode(asio::ssl::verify_none);
    ssl_context.set_default_verify_paths();
    ssl_context.set_verify_callback(
            [](bool preverified, asio::ssl::verify_context &ctx) {
                char subject_name[256];
                X509 *cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
                X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
                std::cout << "Verifying: " << subject_name << std::endl;

                return preverified;
            });

    tcp::resolver resolver(io_context);
    auto endpoint = resolver.resolve(host, port);
    socket = tcp::socket(io_context);
    do_connect(endpoint);

    context_thread.emplace(std::thread([this, self]() {
        io_context.run();
    }));

    send_thread.emplace(std::thread([this, self]() {
        waitSessionStarted();

        if (!errored) {
            auto pSession = session->get();
            while (!pSession->closed) {
                pSession->sendCurrentPackets();
            }
        }
    }));

}

bool ClientConnectionManager::waitSessionStarted() const {
    while (!session.has_value() && !errored) {}
    return !errored;
}

void ClientConnectionManager::executePackets() {
    if (!session.has_value()) return;

    auto pSession = session->get();

    while (!pSession->received.empty()) {
        CommonPacket *received;
        if (pSession->received.pop(received)) {
            received->onReceived(server_endpoint.value());
            delete received;
            received = nullptr;
        }
    }
}

void ClientConnectionManager::addToSendingQueue(CommonPacket *packet) {
    if (!session.has_value()) return;
    session.value()->addToSendingQueue(packet);
}

void ClientConnectionManager::do_connect(const ip::basic_resolver<tcp, asio::any_io_executor>::results_type &endpoint) {
    auto ssl_sock = std::make_shared<ssl_socket>(std::move(socket.value()), ssl_context);

    asio::async_connect(
            ssl_sock->lowest_layer(), endpoint,
            std::bind(&ClientConnectionManager::on_connect, shared_from_this(), ssl_sock, _1, _2)
    );
}


void ClientConnectionManager::on_connect(
        const std::shared_ptr<ssl_socket> &ssl,
        const system::error_code &error, const tcp::endpoint &
) {
    if (!error) {
        server_endpoint.emplace(ssl->lowest_layer().remote_endpoint());
        ssl->async_handshake(
                asio::ssl::stream_base::client,
                std::bind(&ClientConnectionManager::on_handshake, shared_from_this(), ssl, _1)
        );
    } else {
        std::cerr << "Connection error: " << error.message() << std::endl;
        errored = true;
    }
}

void ClientConnectionManager::on_handshake(
        const std::shared_ptr<ssl_socket> &ssl,
        const system::error_code &error
) {
    if (!error) {
        auto server = std::make_shared<NetworkSession>(ssl);
        server->startReceivingPackets();
        session.emplace(server);
    } else {
        auto string = error.message();
        std::cerr << "Handshake error: " << string << std::endl;
        errored = true;
    }
}

