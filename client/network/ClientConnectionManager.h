#pragma once

#include "../../shared/NetworkSession.h"
#include <boost/asio/ts/buffer.hpp>
#include <boost/asio/ts/internet.hpp>
#include <boost/asio/ssl.hpp>
#include <unordered_set>
#include <iostream>
#include <algorithm>
#include <memory>

namespace asio = boost::asio;
namespace ip = asio::ip;
using ip::tcp;

class ClientConnectionManager : public std::enable_shared_from_this<ClientConnectionManager> {
public:
    void connectAsync(const std::string &host, const std::string &port);

    bool waitSessionStarted() const;

    void executePackets();

    void addToSendingQueue(CommonPacket *packet);

private:

    void do_connect(const tcp::resolver::results_type &endpoint);

    void on_connect(
            const std::shared_ptr<ssl_socket> &ssl,
            const system::error_code &error,
            const tcp::endpoint &
    );

    void on_handshake(
            const std::shared_ptr<ssl_socket> &ssl,
            const system::error_code &error
    );

    asio::io_context io_context;
    asio::ssl::context ssl_context{asio::ssl::context::tlsv12_client};
    std::optional<tcp::socket> socket;
    std::optional<std::thread> context_thread;
    std::optional<std::thread> send_thread;
    std::optional<ip::basic_endpoint<tcp>> server_endpoint;
    std::optional<std::shared_ptr<NetworkSession>> session;
    bool errored = false;
};
