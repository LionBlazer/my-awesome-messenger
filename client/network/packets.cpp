#include "../../shared/packets/LogInToServerPacket.h"
#include "../../shared/packets/SendMessagePacket.h"
#include "../../shared/packets/SyncChatsToClientPacket.h"
#include "../../shared/packets/CreateChatToServerPacket.h"
#include "../../shared/packets/AddUserToChatToServerPacket.h"
#include "../../shared/packets/RequestActiveUsersToServerPacket.h"
#include "../../shared/packets/SendActiveUsersToClientPacket.h"
#include "../../shared/packets/OnCreatedChatToClientPacket.h"
#include "../../shared/packets/StartChatWithToServerPacket.h"
#include "../Client.h"

void LogInToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void SendMessagePacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &data = Client::instance().messengerGui->userData;
    for (auto &chat: data.chats) {
        if (chat.chat_id == chat_id) {
            chat.received.push_back(message);
            if (chat_id == data.opened_chat_id) {
                Client::instance().messengerGui->scrolled_to_last = true;
            }
        }
    }
}

void CreateChatToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void AddUserToChatToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {};

void SyncChatsToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &data = Client::instance().messengerGui->userData;

    data.chats = chats;
    data.loggedIn = true;
    data.logging = false;
}

void RequestActiveUsersToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void SendActiveUsersToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &data = Client::instance().messengerGui->userData;
    data.activeUsers = users;
}

void OnCreatedChatToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &data = Client::instance().messengerGui->userData;
    auto val = Chat();
    val.chat_id = chatId;
    data.chats.push_back(val);
}

void StartChatWithToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}