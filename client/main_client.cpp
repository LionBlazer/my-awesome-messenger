#include "../shared/CommonPacket.h"
#include "../shared/NetworkSession.h"
#include "../shared/PacketRegistry.h"
#include "Client.h"
#include <boost/asio/ts/internet.hpp>
#include <iostream>
#include <optional>

namespace asio = boost::asio;
using boost::asio::ip::tcp;

std::string api = "localhost";
std::string port = "8080";
std::string userid = "user1";
std::string password = "23456";

int main(int argc, char *argv[]) {
    try {
        auto& messengerGui = Client::instance().messengerGui;

        messengerGui->startAsync();

        PacketRegistry::registerPackets();

        auto& connectionManager = Client::instance().connectionManager;

        connectionManager->connectAsync(api, port);
        bool connected = connectionManager->waitSessionStarted();

        if (!connected) return -1;

        while (!messengerGui->closed) {
            connectionManager->executePackets();
        }
    } catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    return 0;
}
