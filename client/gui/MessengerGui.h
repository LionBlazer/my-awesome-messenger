#pragma once

#include "MessengerGuiData.h"
#include "immapp/immapp.h"
#include "imgui_md_wrapper.h"
#include "imgui.h"
#include <cmath>
#include <chrono>
#include <memory>

class MessengerGui : public std::enable_shared_from_this<MessengerGui> {
public:

    void startAsync();

    std::string opened_user_chat{};
    bool closed = false;
    MessengerGuiData userData{};
    bool scrolled_to_last = false;
private:
    void setup_style();

    void load_fonts();

    void draw_logo();

    void draw_gui();

    char userid[20] = "";
    char password[20] = "";

    char message[500] = "";

    std::optional<std::thread> gui_thread;
    ImFont *scaled_font = nullptr;
};
