#pragma once
#include "../../shared/user/Chat.h"
#include <string>

struct MessengerGuiData {
    bool logging = false;
    bool loggedIn = false;

    std::string userid;

    std::vector<std::string> activeUsers{};

    int8_t opened_chat_id = -1;
    std::vector<Chat> chats{};
};