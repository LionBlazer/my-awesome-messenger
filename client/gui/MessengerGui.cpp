#include "MessengerGui.h"
#include "../Client.h"
#include "../../shared/packets/LogInToServerPacket.h"
#include "../../shared/packets/RequestActiveUsersToServerPacket.h"
#include "../../shared/packets/CreateChatToServerPacket.h"
#include "../../shared/packets/StartChatWithToServerPacket.h"
#include "../../shared/packets/SendMessagePacket.h"


struct SimpleRunnerParams {
    HelloImGui::VoidFunction guiFunction = HelloImGui::EmptyVoidFunction();
    HelloImGui::VoidFunction beforeExit = HelloImGui::EmptyVoidFunction();
    HelloImGui::VoidFunction loadFonts = HelloImGui::EmptyVoidFunction();
    HelloImGui::VoidFunction setupStyle = HelloImGui::EmptyVoidFunction();

    HelloImGui::RunnerParams ToRunnerParams() {
        auto &self = *this;
        HelloImGui::RunnerParams r;

        r.callbacks.ShowGui = self.guiFunction;
        r.callbacks.BeforeExit = self.beforeExit;
        r.callbacks.LoadAdditionalFonts = self.loadFonts;
        r.callbacks.SetupImGuiStyle = self.setupStyle;

        r.appWindowParams.windowGeometry.size = HelloImGui::DefaultWindowSize;
        r.appWindowParams.windowGeometry.sizeAuto = false;
        r.appWindowParams.restorePreviousGeometry = false;

        r.appWindowParams.windowTitle = "Awesome Messenger";

        r.fpsIdling.fpsIdle = 144;
        r.fpsIdling.enableIdling = false;
        r.emscripten_fps = 144;

        return r;
    }
};


void MessengerGui::startAsync() {
    auto self(shared_from_this());

    gui_thread.emplace(std::thread([this, self]() {
        SimpleRunnerParams params;
        params.setupStyle = [this] { this->setup_style(); };
        params.loadFonts = [this] { this->load_fonts(); };
        params.guiFunction = [this] { this->draw_gui(); };
        params.beforeExit = [this] { closed = true; };

        ImmApp::AddOnsParams addOnsParams;
        addOnsParams.withMarkdown = true;
        addOnsParams.withImplot = true;

        auto resParams = params.ToRunnerParams();

        ImmApp::Run(resParams, addOnsParams);
    }));
}

void MessengerGui::setup_style() {
    ImGui::StyleColorsDark();
}

void MessengerGui::load_fonts() {
    scaled_font = HelloImGui::LoadFontTTF_WithFontAwesomeIcons("fonts/Capture_it.ttf", 21, false);
    ImGui::GetIO().FontDefault = HelloImGui::LoadFontTTF_WithFontAwesomeIcons("fonts/DroidSans.ttf", 15, false);
}

void MessengerGui::draw_logo() {
    static ImVec4 blue_color = ImVec4(0.1f, 0.45f, 0.1f, 1.0f);
    static ImVec4 white_color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
    static float corner_radius = 12.0f;
    static float square_size = 60.0f;

    ImGui::PushFont(scaled_font);
    ImVec2 rect_min = ImGui::GetCursorScreenPos();
    ImVec2 rect_max = ImVec2(rect_min.x + square_size, rect_min.y + square_size);

    ImGui::GetWindowDrawList()->AddRectFilled(rect_min, rect_max, ImGui::ColorConvertFloat4ToU32(blue_color),
                                              corner_radius);
    ImVec2 text_size = ImGui::CalcTextSize("AM");
    ImVec2 text_pos = ImVec2(rect_min.x + (rect_max.x - rect_min.x - text_size.x) * 0.5f,
                             rect_min.y + (rect_max.y - rect_min.y) * 0.5f - text_size.y / 2);
    ImGui::GetWindowDrawList()->AddText(text_pos, ImGui::ColorConvertFloat4ToU32(white_color), "AM");

    ImGui::Dummy(ImVec2(square_size, square_size));
    ImGui::PopFont();
}

void MessengerGui::draw_gui() {
    ImGui::Indent();

    ImGui::BeginChild("Test");
    if (userData.loggedIn) {
        ImGui::NewLine();
        draw_logo();
        ImGui::SameLine();

        auto& cm = Client::instance().connectionManager;

        ImGui::Button("Search Users");
        if (ImGui::IsItemClicked()) {
            ImGui::OpenPopup("active_users");
            cm->addToSendingQueue(new RequestActiveUsersToServerPacket());
        }

        if (ImGui::BeginPopup("active_users")) {
            for (const std::string& name : userData.activeUsers) {
                if (name == userData.userid) continue;
                if (ImGui::Selectable(name.c_str())) {
                    cm->addToSendingQueue(new StartChatWithToServerPacket(name));
                }
            }
            ImGui::EndPopup();
        }

        ImGui::NewLine();
    }

    auto avail = ImGui::GetContentRegionAvail();
    avail.x -= ImGui::GetStyle().IndentSpacing;
    avail.y -= ImGui::GetStyle().IndentSpacing;

    if (userData.loggedIn) {
        avail.x *= 0.25;
        
        ImGui::BeginChild("Context", avail);

        auto& cm = Client::instance().connectionManager;
        if (ImGui::Button("Create Chat")) {
            cm->addToSendingQueue(new CreateChatToServerPacket());
        }


        for (const auto& chat : userData.chats) {
            ImGui::Text("%s", ("Chat" + std::to_string(chat.chat_id)).c_str());
            if (ImGui::IsItemClicked()) {
                userData.opened_chat_id = chat.chat_id;
            }
        }

        ImGui::EndChild();

        ImGui::SameLine();

        ImGui::BeginGroup();
        if (userData.opened_chat_id >= 0) {
            ImGui::Text("Opened Chat ");
            ImGui::SameLine(0, 0);
            ImGui::Text("%hhd", userData.opened_chat_id);
        }

        ImGui::BeginChild("Chat", ImGui::GetContentRegionAvail());

        if (userData.opened_chat_id >= 0) {
            for (const auto& c : userData.chats) {
                if (c.chat_id != userData.opened_chat_id) continue;
                auto s = ImGui::GetContentRegionAvail();
                s.y -= 30 + ImGui::GetTextLineHeightWithSpacing();
                ImGui::BeginChild("ChatContent", s);

                for (const auto& message : c.received) {
                    ImGui::Text("%s", message.senderId.c_str());
                    ImGui::SameLine(0, 0);
                    ImGui::Text(" : ");
                    ImGui::SameLine(0, 0);
                    ImGui::Text("%s", message.message.c_str());
                }
                if (scrolled_to_last) {
                    scrolled_to_last = false;
                    ImGui::SetScrollHereY(0);
                }
                ImGui::EndChild();

                ImGui::SetNextItemWidth(250.f);
                ImGui::InputText("Message", message, IM_ARRAYSIZE(message));
                std::string message_str = message;
                ImGui::SameLine();
                if ((ImGui::Button("Send") || (ImGui::IsKeyPressed(ImGuiKey_Enter)))) {
                    if (!message_str.empty()) {
                        ChatMessage chatMessage;
                        chatMessage.message = message_str;
                        chatMessage.senderId = userData.userid;
                        chatMessage.messageId = c.received.size();

                        cm->addToSendingQueue(new SendMessagePacket(c.chat_id, chatMessage));
                        std::memset(message, '\0', sizeof(message));
                    }
                }
            }
        }

        ImGui::EndChild();
        ImGui::EndGroup();

    } else if (userData.logging) {
        const ImVec2 &tSize = ImGui::CalcTextSize("Loading...");
        avail.x /= 2;
        avail.y /= 2;
        avail.x -= tSize.x / 2;
        avail.y -= tSize.y / 2;
        ImGui::SetCursorPos(avail);
        ImGui::Text("Loading...");
    } else {
        avail.x /= 2;
        avail.y /= 2;
        avail.x -= 250 / 2;
        avail.y -= ImGui::GetTextLineHeightWithSpacing() * 2;
        ImGui::SetCursorPos(avail);

        ImGui::SetNextItemWidth(250.f);
        ImGui::InputText("Login", userid, IM_ARRAYSIZE(userid));
        ImGui::SetCursorPosX(avail.x);

        ImGui::SetNextItemWidth(250.f);
        ImGui::InputText("Password", password, IM_ARRAYSIZE(password), ImGuiInputTextFlags_Password);
        ImGui::SetCursorPosX(avail.x);

        if (ImGui::Button("Log In", ImVec2(250.0f, ImGui::GetTextLineHeightWithSpacing()))) {
            userData.logging = true;
            auto& cm = Client::instance().connectionManager;
            std::string cUser = userid;
            std::string cPass = password;

            userData.userid = userid;

            std::cout << "data: " << cUser << std::endl;
            cm->addToSendingQueue(new LogInToServerPacket(cUser, cPass));
        }
    }

    ImGui::Unindent();

    ImGui::EndChild();
}
