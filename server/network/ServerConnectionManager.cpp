#include "ServerConnectionManager.h"

using namespace std::placeholders;

ServerConnectionManager::ServerConnectionManager()
        : ssl_context_(asio::ssl::context::tlsv12_server) {
    ssl_context_.set_options(
            asio::ssl::context::default_workarounds
            | asio::ssl::context::no_sslv3
            | asio::ssl::context::single_dh_use);
    ssl_context_.use_certificate_chain_file("server.crt");
    ssl_context_.use_private_key_file("server.key", asio::ssl::context::pem);
    ssl_context_.use_tmp_dh_file("dh2048.pem");
}

void ServerConnectionManager::startAsync(short port) {
    auto self(shared_from_this());

    auto endpoint = tcp::endpoint(tcp::v4(), port);
    acceptor_.emplace(tcp::acceptor(io_context, endpoint));
    start_accepting();

    context_thread.emplace(std::thread([this, self]() {
        io_context.run();
    }));

    send_thread.emplace(std::thread([this, self]() {
        while (true) {
            boost::lock_guard<boost::mutex> lock(session_mtx);
            for (const auto &session: clients_sessions) {
                if (session.second) {
                    session.second->sendCurrentPackets();
                }
            }
        }
    }));
}


std::vector<ip::basic_endpoint<tcp>> ServerConnectionManager::removeInactiveConnections() {
    std::vector<ip::basic_endpoint<tcp>> result;

    for (auto it = clients_sessions.begin(); it != clients_sessions.end();) {
        if ((*it).second->closed) {
            result.push_back((*it).first);
            it = clients_sessions.erase(it);
        } else {
            ++it;
        }
    }
    return result;
}

void ServerConnectionManager::executePackets() {
    std::vector<std::pair<ip::basic_endpoint<tcp>, CommonPacket*>> toExecute;

    {
        boost::lock_guard<boost::mutex> lock(session_mtx);
        for (const auto &session: clients_sessions) {
            if (!session.second->closed && !session.second->received.empty()) {
                CommonPacket *received;
                if (session.second->received.pop(received)) {
                    toExecute.emplace_back(session.first, received);
                }
            }
        }
    }

    for (std::pair<ip::basic_endpoint<tcp>, CommonPacket*> packet : toExecute) {
        packet.second->onReceived(packet.first);
        delete packet.second;
        packet.second = nullptr;
    }
}

void ServerConnectionManager::addToSendingQueue(const ip::basic_endpoint<tcp> &to, CommonPacket *packet) {
    boost::lock_guard<boost::mutex> lock(session_mtx);
    auto client = clients_sessions[to];
    if (client) {
        client->addToSendingQueue(packet);
    }
}


void ServerConnectionManager::start_accepting() {
    acceptor_.value().async_accept([this](system::error_code ec, tcp::socket socket) {
        if (!ec) {
            boost::lock_guard<boost::mutex> lock(session_mtx);
            auto endpoint = socket.remote_endpoint();
            std::cout << "Registered connection from " << endpoint << std::endl;
            auto ssl_sock = std::make_shared<ssl_socket>(std::move(socket), ssl_context_);

            ssl_sock->async_handshake(
                    asio::ssl::stream_base::server,
                    std::bind(&ServerConnectionManager::on_handshake, shared_from_this(), endpoint, ssl_sock, _1)
            );

        }
        start_accepting();
    });
}

void ServerConnectionManager::on_handshake(const ip::basic_endpoint<tcp> &endpoint,
                                           const std::shared_ptr<ssl_socket> &socket,
                                           const system::error_code &error) {
    if (!error) {
        boost::lock_guard<boost::mutex> lock(session_mtx);
        auto client = std::make_shared<NetworkSession>(socket);
        client->startReceivingPackets();
        clients_sessions[endpoint] = client;
    } else {
        std::cerr << "Handshake error: " << error.message() << std::endl;
    }
}


