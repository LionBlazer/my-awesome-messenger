#include "../../shared/packets/LogInToServerPacket.h"
#include "../../shared/packets/SyncChatsToClientPacket.h"
#include "../../shared/packets/SendMessagePacket.h"
#include "../../shared/packets/CreateChatToServerPacket.h"
#include "../../shared/packets/AddUserToChatToServerPacket.h"
#include "../../shared/packets/RequestActiveUsersToServerPacket.h"
#include "../../shared/packets/SendActiveUsersToClientPacket.h"
#include "../../shared/packets/OnCreatedChatToClientPacket.h"
#include "../../shared/packets/StartChatWithToServerPacket.h"
#include "../user/ActiveUser.h"
#include "../Server.h"
#include <iostream>

void LogInToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    std::cout << "User attempting to log in with userid \"" << userid << "\" and password \"" << password << "\""
              << std::endl;
    auto &user_manager = Server::instance().userManager;
    user_manager.tryAuth(from, userid, password, [from, this]() {
        auto &server = Server::instance();
        auto &user_manager = server.userManager;

        auto chats = user_manager.calcUserChats(userid);
        server.connectionManager->addToSendingQueue(from, new SyncChatsToClientPacket(chats));
    });
}

uint32_t createChatFor(const std::string &username) {
    auto &user_manager = Server::instance().userManager;
    uint32_t chatId = user_manager.chats.size();
    const auto &chat = std::make_shared<Chat>();
    chat->chat_id = chatId;
    user_manager.addChat(chat);
    user_manager.addUserInChat(chatId, username);
    return chatId;
}

void CreateChatToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &user_manager = Server::instance().userManager;
    auto id = createChatFor(user_manager.users[from]->username);
    Server::instance().connectionManager->addToSendingQueue(from, new OnCreatedChatToClientPacket(id));
}

void AddUserToChatToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &user_manager = Server::instance().userManager;
    const auto &chat = user_manager.getChat(chatId);
    if (!chat) return;


    auto pUser = user_manager.users[from];

    if (user_manager.isUserInChat(chatId, pUser->username)) return;

    user_manager.addUserInChat(chatId, invited);
}

void SendMessagePacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto user_manager = Server::instance().userManager;
    auto user = user_manager.users[from];
    auto chat = user_manager.getChat(chat_id);
    if (chat) {
        chat->addMessageSafe(message);
        for (const auto &uName: user_manager.getUsersInChat(chat_id)) {
            auto memberEnd = user_manager.findUserById(uName);
            if (!memberEnd) continue;
            Server::instance().connectionManager->addToSendingQueue(
                    memberEnd.value(), new SendMessagePacket(chat_id, message)
            );
        }
    }
}

void SyncChatsToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void RequestActiveUsersToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    std::vector<std::string> collected;

    auto &user_manager = Server::instance().userManager;
    for (const auto &u: user_manager.users) {
        collected.emplace_back(u.second->username);
    }

    auto &server = Server::instance();
    server.connectionManager->addToSendingQueue(from, new SendActiveUsersToClientPacket(collected));

}

void SendActiveUsersToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void OnCreatedChatToClientPacket::onReceived(const ip::basic_endpoint<tcp> &from) {}

void StartChatWithToServerPacket::onReceived(const ip::basic_endpoint<tcp> &from) {
    auto &user_manager = Server::instance().userManager;
    auto id = createChatFor(user_manager.users[from]->username);
    user_manager.addUserInChat(id, invited);
    auto to = user_manager.findUserById(invited);
    if (!to.has_value()) return;

    Server::instance().connectionManager->addToSendingQueue(from, new OnCreatedChatToClientPacket(id));
    Server::instance().connectionManager->addToSendingQueue(to.value(), new OnCreatedChatToClientPacket(id));
}

