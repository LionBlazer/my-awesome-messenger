#pragma once

#include "../../shared/NetworkSession.h"
#include <boost/asio/ts/buffer.hpp>
#include <boost/asio/ts/internet.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_guard.hpp>
#include <boost/asio/ssl.hpp>
#include <unordered_set>
#include <iostream>
#include <algorithm>
#include <memory>

namespace asio = boost::asio;
namespace ip = asio::ip;
using ip::tcp;
using ssl_socket = asio::ssl::stream<ip::tcp::socket>;

class ServerConnectionManager : public std::enable_shared_from_this<ServerConnectionManager> {
public:
    ServerConnectionManager();

    void startAsync(short port);

    std::vector<ip::basic_endpoint<tcp>> removeInactiveConnections();

    void executePackets();

    void addToSendingQueue(const ip::basic_endpoint<tcp> &to, CommonPacket *packet);

private:
    void start_accepting();

    void on_handshake(
            const ip::basic_endpoint<tcp> &endpoint,
            const std::shared_ptr<ssl_socket>& socket,
            const system::error_code &error
    );

    asio::io_context io_context;
    std::optional<tcp::acceptor> acceptor_;
    std::optional<std::thread> context_thread;
    std::optional<std::thread> send_thread;
    std::unordered_map<ip::basic_endpoint<tcp>, std::shared_ptr<NetworkSession>> clients_sessions;
    boost::mutex session_mtx;
    asio::ssl::context ssl_context_;
};