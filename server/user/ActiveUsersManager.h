#pragma once

#include "ActiveUser.h"
#include <boost/asio/ts/internet.hpp>
#include <unordered_map>

namespace ip = boost::asio::ip;
using ip::tcp;

class ActiveUsersManager {
public:
    std::optional<ip::basic_endpoint<tcp>> findUserById(const std::string &userid) {
        for (const auto& u : users) {
            if (u.second->username == userid) {
                return u.first;
            }
        }
        return std::nullopt;
    }

    std::unordered_set<uint32_t> findUserChats(const std::string &userid) {
        if (!userChatsLookup.contains(userid)) {
            auto uChats = std::unordered_set<uint32_t>();
            userChatsLookup[userid] = uChats;
            return uChats;
        }

        return userChatsLookup[userid];
    }

    std::vector<std::shared_ptr<Chat>> calcUserChats(const std::string &userid) {
        std::vector<std::shared_ptr<Chat>> result;
        auto foundChats = findUserChats(userid);
        for (auto fChat : foundChats) {
            result.push_back(getChat(fChat));
        }

        return result;
    }

    void addChat(const std::shared_ptr<Chat>& chat) {
        chats[chat->chat_id] = chat;
    }

    std::shared_ptr<Chat>& getChat(uint32_t chatId) {
        return chats[chatId];
    }

    void addUserInChat(uint32_t chat, const std::string& user) {
        if (!chatUsers.contains(chat)) {
            chatUsers[chat] = std::vector<std::string>();
        }

        if (!userChatsLookup.contains(user)) {
            userChatsLookup[user] = std::unordered_set<uint32_t>();
        }

        chatUsers[chat].push_back(user);
        userChatsLookup[user].insert(chat);
    }

    std::vector<std::string> getUsersInChat(uint32_t chat) {
        return chatUsers[chat];
    }

    bool isUserInChat(uint32_t chat, const std::string& user) {
        return userChatsLookup.contains(user) && userChatsLookup[user].contains(chat);
    }

    void tryAuth(
            const ip::basic_endpoint<tcp> &from,
            const std::string &userid,
            const std::string &password,
            const std::function<void()>& callback
    ) {
        //check password code
        if (findUserById(userid).has_value()) return;

        auto repeatAuth = users[from];
        if (!repeatAuth) {
            users[from] = std::make_shared<ActiveUser>(userid);
        } else {
            users[from] = repeatAuth->shared_from_this();
        }

        callback();
    }

    std::unordered_map<ip::basic_endpoint<tcp>, std::shared_ptr<ActiveUser>> users{};
    std::unordered_map<uint32_t, std::shared_ptr<Chat>> chats{};
private:
    std::unordered_map<uint32_t, std::vector<std::string>> chatUsers{};

    std::unordered_map<std::string, std::unordered_set<uint32_t>> userChatsLookup{};
};
