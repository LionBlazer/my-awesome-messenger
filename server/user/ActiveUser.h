#pragma once

#include <string>
#include <utility>

class ActiveUser : public std::enable_shared_from_this<ActiveUser> {
public:
    ActiveUser() = default;

    explicit ActiveUser(std::string username) : username(std::move(username)) {}

    std::string username;
};