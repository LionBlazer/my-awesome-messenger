#pragma once

#include <string>
#include <vector>
#include "../../shared/user/Chat.h"

struct GlobalUserInfo {
    std::string userid{};
    std::vector<int32_t> chats{};
};