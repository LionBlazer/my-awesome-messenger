#pragma once

#include <string>
#include <unordered_map>
#include "GlobalUserInfo.h"

class GlobalUsersManager {
public:
    GlobalUserInfo registerNewUser(const std::string& userid, const std::string& password) {
        return GlobalUserInfo{};
    }

    GlobalUserInfo loginUser(const std::string& userid, const std::string& password) {
        return GlobalUserInfo{};
    }

    std::vector<GlobalUserInfo> findByPrefix(const std::string& prefix) {
        return std::vector<GlobalUserInfo>();
    }
};