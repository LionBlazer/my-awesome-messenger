#include "../shared/CommonPacket.h"
#include "../shared/NetworkSession.h"
#include "../shared/PacketRegistry.h"
#include "Server.h"
#include <boost/asio/ts/internet.hpp>
#include <unordered_set>
#include <iostream>

namespace asio = boost::asio;
using boost::asio::ip::tcp;

int main() {
    try {
        PacketRegistry::registerPackets();

        Server::instance().connectionManager->startAsync(8080);

        while (true) {
            const auto& removedEndpoints =  Server::instance().connectionManager->removeInactiveConnections();
            for (const auto& endpoint : removedEndpoints) {
                Server::instance().userManager.users.erase(endpoint);
            }

            Server::instance().connectionManager->executePackets();
        }
    } catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    return 0;
}
