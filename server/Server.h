#pragma once

#include "network/ServerConnectionManager.h"
#include "user/ActiveUsersManager.h"
#include <memory>

struct Server {
    static Server& instance() {
        static Server server;
        return server;
    }

    std::shared_ptr<ServerConnectionManager> connectionManager = std::make_shared<ServerConnectionManager>();
    ActiveUsersManager userManager = ActiveUsersManager();
};